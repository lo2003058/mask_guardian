import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mask_guardian/cubit/app_cubits.dart';
import 'package:mask_guardian/widgets/app_large_text.dart';
import 'package:mask_guardian/widgets/app_text.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../widgets/color_list.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  final controller = PageController();
  bool isLastPage = false;

  @override
  void discpose() {
    controller.dispose();

    super.dispose();
  }

  List images = [
    "welcome1.0.png",
    "welcome2.1.png",
    "welcome3.2.png",
    "welcome4.0.png",
  ];

  List title = [
    "Mask Guardian",
    "Alert Message",
    "Mask Detection",
    "User System",
  ];

  List content = [
    "Using machine learning distinguish people correctly wearing masks",
    "Receive alert message to remind administrator and warn offenders",
    "Using high-end technology and high accuracy model to analyze",
    "Each user have unique id to receive alert message",
  ];

  getContainer(index) {
    return Container(
      width: double.maxFinite,
      height: double.maxFinite,
      decoration: BoxDecoration(
          image: DecorationImage(
              image:
                  AssetImage("assets/images/welcome_images/" + images[index]),
              fit: BoxFit.cover)),
      child: Container(
        margin: const EdgeInsets.only(top: 150, left: 20, right: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(children: [
                  AppLargeText(text: title[index]),
                ]),
                SizedBox(height: 20),
                Container(
                  width: 300,
                  child: AppText(
                    text: content[index],
                    size: 16,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.only(bottom: 80),
        child: PageView.builder(
            onPageChanged: (index) {
              setState(() {
                isLastPage = index == images.length - 1;
              });
            },
            controller: controller,
            scrollDirection: Axis.horizontal,
            itemCount: images.length,
            itemBuilder: (_, index) {
              return getContainer(index);
            }),
      ),
      bottomSheet: isLastPage
          ? TextButton.icon(
              icon: Icon(Icons.check,color: ColorList.mainColor),
              onPressed: () async {
                final prefs = await SharedPreferences.getInstance();
                prefs.setBool("isFirstStart", true);
                BlocProvider.of<AppCubits>(context).getNotice(7);
              },
              label: Text(
                "Start to use",
                style: TextStyle(
                  color: ColorList.mainColor,
                ),
              ),
              style: TextButton.styleFrom(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  minimumSize: const Size.fromHeight(80)),
            )
          : Container(
              padding: const EdgeInsets.symmetric(horizontal: 1, vertical: 1),
              height: 80,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.skip_next,
                    ),
                    color: ColorList.mainColor,
                    onPressed: () => controller.animateToPage(
                      images.length - 1,
                      duration: const Duration(milliseconds: 500),
                      curve: Curves.easeInOut,
                    ),
                  ),
                  Center(
                    child: SmoothPageIndicator(
                      controller: controller,
                      count: images.length,
                      effect: WormEffect(
                        type: WormType.thin,
                        spacing: 8,
                        dotColor: Colors.grey,
                        activeDotColor: ColorList.mainColor,
                      ),
                      onDotClicked: (index) => controller.animateToPage(
                        index,
                        duration: const Duration(milliseconds: 500),
                        curve: Curves.easeInOut,
                      ),
                    ),
                  ),
                  IconButton(
                    icon: Icon(
                      Icons.navigate_next,
                    ),
                    color: ColorList.mainColor,
                    onPressed: () => controller.nextPage(
                        duration: const Duration(milliseconds: 500),
                        curve: Curves.easeInOut),
                  ),
                ],
              ),
            ),
    );
  }
}
