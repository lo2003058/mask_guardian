import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:mask_guardian/models/notice4api.dart';
import '../widgets/app_text.dart';
import '../widgets/color_list.dart';

class NoticeInfo extends StatefulWidget {
  final int noticeId;

  const NoticeInfo({
    Key? key,
    required this.noticeId,
  }) : super(key: key);

  @override
  State<NoticeInfo> createState() => _NoticeInfoState();
}

class _NoticeInfoState extends State<NoticeInfo> {
  String baseurl = "http://192.168.50.48:8000/admin/api/";

  late Future<Notice> futureNotice;

  @override
  void initState() {
    super.initState();
    futureNotice = getNoticeById(widget.noticeId);
  }

  Future<Notice> getNoticeById(noticeId) async {
    var apiUrl = "notice/";
    http.Response res =
        await http.get(Uri.parse(baseurl + apiUrl + noticeId.toString()));
    if (res.statusCode == 200) {
      return Notice.fromJson(jsonDecode(res.body));
    } else {
      throw Exception('Failed to load album');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Notice Info'),
        backgroundColor: ColorList.mainColor,
      ),
      body: FutureBuilder<Notice>(
        future: futureNotice,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Container(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    Container(
                      height: 200,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.white,
                          image: DecorationImage(
                              image: NetworkImage("http://192.168.50.48:8000/" +
                                  snapshot.data!.media),
                              fit: BoxFit.cover)),
                    ),
                    Container(
                      child: Column(children: [
                        Row(children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                            child: AppText(
                              text: "Location: " + snapshot.data!.location,
                              size: 18,
                            ),
                          ),
                        ]),
                        Row(children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                            child: AppText(
                              text: "With mask: " +
                                  snapshot.data!.with_mask_count.toString(),
                              size: 18,
                              color: Colors.green,
                            ),
                          ),
                        ]),
                        Row(children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                            child: AppText(
                              text: "Mask weared incorrect: " +
                                  snapshot.data!.mask_weared_incorrect_count
                                      .toString(),
                              size: 18,
                              color: Colors.orange,
                            ),
                          ),
                        ]),
                        Row(children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                            child: AppText(
                              text: "Without mask: " +
                                  snapshot.data!.without_mask_count.toString(),
                              size: 18,
                              color: Colors.red,
                            ),
                          ),
                        ]),
                        Row(children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                            child: AppText(
                              text: "Total: " +
                                  snapshot.data!.total_count.toString(),
                              size: 18,
                            ),
                          ),
                        ]),
                        Row(children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                            child: AppText(
                              text: "Created at: " + snapshot.data!.created_at,
                              size: 18,
                            ),
                          ),
                        ]),
                      ]),
                    ),
                  ],
                ),
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          // By default, show a loading spinner.
          return const CircularProgressIndicator();
        },
      ),
    );
  }
}
