import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:mask_guardian/nav_pages/about.dart';
import 'package:mask_guardian/nav_pages/home.dart';
import 'package:mask_guardian/nav_pages/alert.dart';
import 'package:mask_guardian/nav_pages/user.dart';

import '../widgets/app_large_text.dart';
import '../widgets/color_list.dart';

class HomeNav extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BottomNavigationController(),
    );
  }
}

class BottomNavigationController extends StatefulWidget {
  BottomNavigationController({Key? key}) : super(key: key);

  @override
  _BottomNavigationControllerState createState() =>
      _BottomNavigationControllerState();
}

class _BottomNavigationControllerState
    extends State<BottomNavigationController> {
  final navigationKey = GlobalKey<CurvedNavigationBarState>();

  //目前選擇頁索引值
  int _currentIndex = 0; //預設值
  final pages = [
    HomePage(),
    AlertPage(),
    UserPage(),
    AboutPage()
  ];
  final title = [
    AppLargeText(text: "Home"),
    AppLargeText(text: "Alert"),
    AppLargeText(text: "User"),
    AppLargeText(text: "About")
  ];

  final items = <Widget>[
    Icon(Icons.home, size: 30),
    Icon(Icons.add_alert_rounded, size: 30),
    Icon(Icons.account_circle, size: 30),
    Icon(Icons.question_mark, size: 30),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          //title and user icon
          Container(
            padding: const EdgeInsets.only(top: 70, left: 20),
            child: Row(
              children: [
                title[_currentIndex],
                Expanded(child: Container()),
                Container(
                  margin: const EdgeInsets.only(right: 20),
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.grey.withOpacity(.5),
                    image: DecorationImage(
                        image: AssetImage("assets/images/icons/user_icon2.jpg"),
                        fit: BoxFit.cover),
                  ),
                  child: InkWell(
                    onTap: () {
                      final navigationState = navigationKey.currentState!;
                      navigationState.setPage(2);
                    },
                  ),
                )
              ],
            ),
          ),
          Expanded(child: pages[_currentIndex]),
        ],
      ),
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
          iconTheme: IconThemeData(color: Colors.white),
        ),
        child: CurvedNavigationBar(
          key: navigationKey,
          color: ColorList.mainColor,
          backgroundColor: Colors.transparent,
          animationDuration: Duration(milliseconds: 300),
          items: items,
          index: _currentIndex,
          onTap: _onItemClick,
        ),
      ),
    );
  }

  void _onItemClick(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
