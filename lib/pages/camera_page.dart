import 'dart:isolate';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:mask_guardian/widgets/color_list.dart';

class CameraPage extends StatefulWidget {
  List<CameraDescription>? cameras;

  // fix this
  String url;
  int minutes;

  CameraPage({required this.url, required this.minutes, this.cameras, Key? key})
      : super(key: key);

  @override
  _CameraPageState createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage> {
  late CameraController controller;
  CameraImage? cameraImage;
  bool detectionStatus = false;
  final receivePort = ReceivePort();

  // final label = TfLife().loadLabels();

  @override
  void initState() {
    super.initState();
    controller = CameraController(widget.cameras![0], ResolutionPreset.high,
        imageFormatGroup: ImageFormatGroup.yuv420, enableAudio: false);
    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {
        controller.startImageStream((imageStream) {
          cameraImage = imageStream;
          if (detectionStatus && cameraImage != null) {
            // runModel();
          }
        });
      });
    });
    controller.lockCaptureOrientation();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!controller.value.isInitialized) {
      return const SizedBox(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }

    return Scaffold(
      body: Column(children: [
        Container(
          padding: const EdgeInsets.only(top: 40),
          child: Text(widget.url),
        ),
        Expanded(
          child: CameraPreview(controller),
        ),
        Container(
          padding: const EdgeInsets.only(top: 10, bottom: 40),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              IconButton(
                icon: Icon(
                  Icons.arrow_back_ios_outlined,
                ),
                color: ColorList.mainColor,
                onPressed: () => {Navigator.pop(context)},
              ),
              IconButton(
                icon: Icon(
                  Icons.notifications_active_outlined,
                ),
                color: ColorList.mainColor,
                onPressed: () => {},
              ),
              IconButton(
                icon: Icon(
                  Icons.face_outlined,
                ),
                color: ColorList.mainColor,
                onPressed: () {
                  setState(() {
                    detectionStatus == true
                        ? detectionStatus = false
                        : detectionStatus = true;
                  });
                  print(detectionStatus);
                },
              )
            ],
          ),
        ),
      ]),
    );
  }
}
