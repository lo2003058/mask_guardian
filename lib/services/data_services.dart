import 'dart:convert';
import 'package:http/http.dart' as http;
import '../models/notice.dart';

class DataServices {
  String baseurl = "http://192.168.50.48:8000/api/";

  Future<List<Notice>> getNotice(getDataCount) async {
    var apiUrl = "findNoticeByLimit";
    http.Response res = await http
        .get(Uri.parse(baseurl + apiUrl + "?limit=" + getDataCount.toString() + "&page=1"));
    try {
      if (res.statusCode == 200) {
        List<dynamic> list = json.decode(res.body);
        return list.map((e) => Notice.fromJson(e)).toList();
      } else {
        return <Notice>[];
      }
    } catch (e) {
      print(e);
      return <Notice>[];
    }
  }

}
