import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:mask_guardian/cubit/app_cubit_states.dart';
import 'package:mask_guardian/cubit/app_cubits.dart';
import 'package:mask_guardian/widgets/app_large_text.dart';
import 'package:mask_guardian/widgets/banner.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../main.dart';
import '../models/banner_model.dart';
import '../pages/notice_info.dart';
import '../widgets/color_list.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final navigationKey = GlobalKey<CurvedNavigationBarState>();
  final RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  List<BannerModel> banner = [];
  int banner_index = 0;
  String baseurl = "http://192.168.50.48:8000/api/";
  var notice;

  // List<Notice> notice = [];

  @override
  void initState() {
    super.initState();

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
          notification.hashCode,
          notification.title,
          notification.body,
          NotificationDetails(
            android: AndroidNotificationDetails(
              channel.id,
              channel.name,
              importance: Importance.max,
              priority: Priority.high,
              color: ColorList.mainColor,
              playSound: true,
              largeIcon:
                  FilePathAndroidBitmap(message.data['image'].toString()),
              icon: '@mipmap/ic_launcher',
              styleInformation: BigTextStyleInformation(
                  notification.body.toString(),
                  htmlFormatBigText: true,
              ),
            ),
          ),
        );
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      if (notification != null && android != null) {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  NoticeInfo(noticeId: int.parse(message.data['id']))),
        );

        // showDialog(
        //     context: context,
        //     builder: (_) {
        //       return AlertDialog(
        //         title: Text(notification.title.toString()),
        //         content: SingleChildScrollView(
        //           child: Column(
        //             crossAxisAlignment: CrossAxisAlignment.start,
        //             children: [Text(notification.body.toString())],
        //           ),
        //         ),
        //       );
        //     });

      }
    });
  }

  void testNotification() {
    flutterLocalNotificationsPlugin.show(
        0,
        'testing',
        'hello *n',
        NotificationDetails(
            android: AndroidNotificationDetails(
          channel.id,
          channel.name,
          importance: Importance.high,
          color: ColorList.mainColor,
          playSound: true,
          icon: '@mipmap/ic_launcher',
        )));
  }

  Future<void> _onRefresh(context) {
    return Future.delayed(Duration(seconds: 1), () {
      //  Delay 2s Complete refresh
      setState(() {
        BlocProvider.of<AppCubits>(context).updateNotice(7);
        _refreshController.refreshCompleted();
      });
    });
  }

  Future<void> _onLoading() {
    return Future.delayed(Duration(seconds: 1), () {
      //  Delay 2s Complete refresh
      setState(() {
        _refreshController.loadComplete();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BannerWidget(),
          //notice
          Container(
            margin: const EdgeInsets.only(left: 20),
            child: AppLargeText(
              text: "Notice",
              size: 22,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          BlocBuilder<AppCubits, CubitStates>(builder: (context, state) {
            if (state is LoadedStatus) {
              notice = state.notice;
              return Expanded(
                child: MediaQuery.removePadding(
                    removeTop: true,
                    context: context,
                    child: notice.isEmpty
                        ? Center(child: CircularProgressIndicator())
                        : SmartRefresher(
                            enablePullDown: true,
                            enablePullUp: false,
                            header: WaterDropHeader(),
                            controller: _refreshController,
                            onRefresh: () async {
                              _onRefresh(context);
                            },
                            onLoading: _onLoading,
                            child: ListView.builder(
                                // controller: ScrollController(),
                                scrollDirection: Axis.vertical,
                                shrinkWrap: true,
                                itemCount: notice.length,
                                itemBuilder: (context, index) {
                                  return Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                    child: Card(
                                      color: Colors.grey[200],
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20)),
                                      child: ListTile(
                                        onTap: () {
                                          // showNotification();

                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    NoticeInfo(
                                                        noticeId:
                                                            notice[index].id)),
                                          );
                                        },
                                        title: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              0, 10, 0, 0),
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 1, vertical: 1),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text(notice[index].location)
                                              ],
                                            ),
                                          ),
                                        ),
                                        subtitle: Container(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 1, vertical: 1),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            children: [
                                              Text(notice[index].created_at),
                                              Chip(
                                                label: Text(notice[index]
                                                    .with_mask_count
                                                    .toString()),
                                                backgroundColor: Colors.green,
                                                elevation: 3.0,
                                              ),
                                              Chip(
                                                label: Text(notice[index]
                                                    .mask_weared_incorrect_count
                                                    .toString()),
                                                backgroundColor: Colors.orange,
                                                elevation: 3.0,
                                              ),
                                              Chip(
                                                label: Text(notice[index]
                                                    .without_mask_count
                                                    .toString()),
                                                backgroundColor: Colors.red,
                                                elevation: 3.0,
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  );
                                }),
                          )),
              );
            } else {
              return Container();
            }
          }),
        ],
      ),
    );
  }
}
