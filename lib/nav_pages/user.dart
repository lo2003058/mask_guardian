import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../widgets/app_text.dart';

class UserPage extends StatefulWidget {
  @override
  State<UserPage> createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          // background image and bottom contents
          Column(
            children: <Widget>[
              Container(
                height: 200.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image:
                        AssetImage("assets/images/background/background1.jpg"),
                    fit: BoxFit.cover,
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.3),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0.0, 5.0), // changes position of shadow
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image:
                      AssetImage("assets/images/background/background3.jpeg"),
                      fit: BoxFit.cover,
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(20, 35, 20, 0),
                    child: Column(children: [
                      Container(
                        child: Column(children: [
                          Center(
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                              child: AppText(
                                text: "@yin7414",
                                size: 22,
                              ),
                            ),
                          ),
                          Row(children: [
                            Padding(
                              padding: EdgeInsets.fromLTRB(35, 25, 0, 0),
                              child: AppText(
                                text: "96051390",
                                size: 18,
                              ),
                            ),
                          ]),
                          Row(children: [
                            Padding(
                              padding: EdgeInsets.fromLTRB(35, 15, 0, 0),
                              child: AppText(
                                text: "217205588@stu.vtc.edu.hk",
                                size: 18,
                              ),
                            ),
                          ]),
                          Row(children: [
                            Padding(
                              padding: EdgeInsets.fromLTRB(35, 15, 0, 0),
                              child: AppText(
                                text: "Admin Role",
                                size: 18,
                              ),
                            ),
                          ]),
                          Row(children: [
                            Padding(
                              padding: EdgeInsets.fromLTRB(35, 15, 0, 0),
                              child: AppText(
                                text: "Join at 2022-05",
                                size: 18,
                              ),
                            ),
                          ]),
                        ]),
                      )
                    ]),
                  ),
                ),
              )
            ],
          ),
          // Profile image
          Positioned(
            top: 100.0, // (background container size) - (circle height / 2)
            child: Container(
              height: 130.0,
              width: 130.0,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage("assets/images/icons/user_icon2.jpg"),
                  fit: BoxFit.cover,
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.8),
                    spreadRadius: 2,
                    blurRadius: 3,
                    offset: Offset(0.0, 5.0), // changes position of shadow
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
