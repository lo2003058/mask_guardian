import 'dart:convert';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:http/http.dart' as http;

import '../models/notice.dart';
import '../pages/notice_info.dart';

class AlertPage extends StatefulWidget {
  @override
  State<AlertPage> createState() => _AlertPageState();
}

class _AlertPageState extends State<AlertPage> {
  final navigationKey = GlobalKey<CurvedNavigationBarState>();
  final RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  String baseurl = "http://192.168.50.48:8000/api/";
  int currentPage = 1;
  int maxGetCount = 10;
  List<Notice> notice = [];

  @override
  void initState() {
    super.initState();
    getNotice(isRefresh: true);
  }

  Future<bool> getNotice({bool isRefresh = false}) async {
    if (isRefresh) {
      if (notice.isNotEmpty) {
        notice = <Notice>[];
        _refreshController.resetNoData();
      }
      currentPage = 1;
    }
    List<Notice> rs;
    var apiUrl = "findNoticeByLimit";
    http.Response res = await http.get(Uri.parse(baseurl +
        apiUrl +
        "?limit=" +
        maxGetCount.toString() +
        "&page=" +
        currentPage.toString()));
    try {
      if (res.statusCode == 200) {
        List<dynamic> list = json.decode(res.body);
        rs = list.map((e) => Notice.fromJson(e)).toList();
        if (isRefresh) {
          notice = rs;
        } else {
          if (rs.isEmpty) {
            return false;
          } else {
            notice.addAll(rs);
          }
        }
        currentPage++;
        setState(() {});
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      SizedBox(
        height: 20,
      ),
      Expanded(
        child: MediaQuery.removePadding(
            removeTop: true,
            context: context,
            child: notice.isEmpty
                ? Center(child: CircularProgressIndicator())
                : SmartRefresher(
                    enablePullDown: true,
                    enablePullUp: true,
                    header: WaterDropHeader(),
                    controller: _refreshController,
                    onRefresh: () async {
                      final result = await getNotice(isRefresh: true);
                      if (result) {
                        Future.delayed(const Duration(seconds: 1), () {
                          setState(() {
                            _refreshController.refreshCompleted();
                          });
                        });
                      } else {
                        _refreshController.refreshFailed();
                      }
                    },
                    onLoading: () async {
                      final result = await getNotice();
                      if (result) {
                        Future.delayed(const Duration(seconds: 1), () {
                          setState(() {
                            _refreshController.loadComplete();
                          });
                        });
                      } else {
                        _refreshController.loadNoData();
                      }
                    },
                    child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: notice.length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: Card(
                              color: Colors.grey[200],
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              child: ListTile(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => NoticeInfo(noticeId: notice[index].id)),
                                  );
                                },
                                title: Padding(
                                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(horizontal: 1, vertical: 1),
                                    child: Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(notice[index].location)
                                      ],
                                    ),
                                  ),
                                ),
                                subtitle: Container(
                                  padding: const EdgeInsets.symmetric(horizontal: 1, vertical: 1),
                                  child: Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                    children: [
                                      Text(notice[index].created_at),
                                      Chip(
                                        label: Text(notice[index]
                                            .with_mask_count
                                            .toString()),
                                        backgroundColor: Colors.green,
                                        elevation: 3.0,
                                      ),
                                      Chip(
                                        label: Text(notice[index]
                                            .mask_weared_incorrect_count
                                            .toString()),
                                        backgroundColor: Colors.orange,
                                        elevation: 3.0,
                                      ),
                                      Chip(
                                        label: Text(notice[index]
                                            .without_mask_count
                                            .toString()),
                                        backgroundColor: Colors.red,
                                        elevation: 3.0,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          );
                        }),
                  )),
      ),
    ]);
  }
}
