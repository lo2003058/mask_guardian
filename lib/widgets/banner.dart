import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mask_guardian/models/banner_model.dart';
import 'package:http/http.dart' as http;

class BannerWidget extends StatefulWidget {
  BannerWidget({
    Key? key,
  }) : super(key: key);

  @override
  State<BannerWidget> createState() => _BannerWidgetState();
}

class _BannerWidgetState extends State<BannerWidget> {
  int banner_index = 0;
  String baseurl = "http://192.168.50.48:8000/api/";
  List<BannerModel> bannerList = [];

  @override
  void initState() {
    super.initState();
    getBanner();
  }

  Future<bool> getBanner() async {
    var apiUrl = "appGetBanner";
    http.Response res = await http.get(Uri.parse(baseurl + apiUrl));
    try {
      if (res.statusCode == 200) {
        List<dynamic> list = json.decode(res.body);
        bannerList = list.map((e) => BannerModel.fromJson(e)).toList();
        setState(() {});
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      SizedBox(
        height: 20,
      ),
      //banner
      Container(
          margin: const EdgeInsets.only(left: 20),
          width: MediaQuery.of(context).size.width - 40,
          height: 200,
          child: PageView.builder(
              itemCount: bannerList.length,
              controller: PageController(),
              onPageChanged: (int index) =>
                  setState(() => banner_index = index),
              itemBuilder: (_, i) {
                return Transform.scale(
                  scale: i == banner_index ? 1 : 0.9,
                  child: Card(
                      elevation: 6,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.white,
                            image: DecorationImage(
                                // image: AssetImage("assets/images/test/test.jpg"),
                                // image: NetworkImage("https://i.imgur.com/zle4Lzb.jpg"),
                                image: NetworkImage("http://192.168.50.48:8000/" + bannerList[i].media),
                                fit: BoxFit.cover)),
                      )),
                );
              })),
      SizedBox(
        height: 20,
      )
    ]);
  }
}
