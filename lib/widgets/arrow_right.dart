import 'package:flutter/material.dart';

class ArrowRight extends StatelessWidget {
  bool? isResponsive;
  double? width;

  ArrowRight({Key? key, this.width, this.isResponsive = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: 60,
      // decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [Image.asset("assets/images/icons/arrow-right-solid.png")],
      ),
    );
  }
}
