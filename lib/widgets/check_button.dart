import 'package:flutter/material.dart';

import 'color_list.dart';

class CheckButton extends StatelessWidget {
  bool? isResponsive;
  double? width;

  CheckButton({Key? key, this.width, this.isResponsive = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton.icon(
      onPressed: () {},
      icon: Icon(
        Icons.check,
        size: 24.0,
      ),
      label: Text('Start to use'),
      style: ElevatedButton.styleFrom(
        primary: ColorList.mainColor,
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(20.0),
        ),
      ),
    );
  }
}
