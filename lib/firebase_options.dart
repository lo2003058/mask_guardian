// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for macos - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyDe3JmdtV127cgjSySZOtabMFR49ecX_C0',
    appId: '1:613440059270:web:75110420736c314bf05e07',
    messagingSenderId: '613440059270',
    projectId: 'itp-project-92e27',
    authDomain: 'itp-project-92e27.firebaseapp.com',
    storageBucket: 'itp-project-92e27.appspot.com',
    measurementId: 'G-HPN1KVBR0X',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyC8uibsnHGNCvLfStUkLdl4LAhWWr-q-AA',
    appId: '1:613440059270:android:17dc364cce0c4d44f05e07',
    messagingSenderId: '613440059270',
    projectId: 'itp-project-92e27',
    storageBucket: 'itp-project-92e27.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyC9rgixEsycxKjY84NC6cJ46QMzjypygmE',
    appId: '1:613440059270:ios:dc2589d3a422cfa7f05e07',
    messagingSenderId: '613440059270',
    projectId: 'itp-project-92e27',
    storageBucket: 'itp-project-92e27.appspot.com',
    iosClientId: '613440059270-9rni786bqbonfec9p796b0r6ebspurg0.apps.googleusercontent.com',
    iosBundleId: 'com.itp.maskDetection',
  );
}
