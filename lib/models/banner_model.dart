import 'dart:convert';

import 'package:intl/intl.dart';

class BannerModel {
  int id;
  String media;

  BannerModel({
    required this.id,
    required this.media,
  });

  factory BannerModel.fromJson(Map<String, dynamic> json) {
    var banner = BannerModel(
      id: json["id"],
      media: json["media"],
    );
    return banner;
  }
}
