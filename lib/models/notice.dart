import 'package:intl/intl.dart';

class Notice {
  int id;
  int mask_weared_incorrect_count;
  int with_mask_count;
  int without_mask_count;
  int total_count;
  String media;
  String location;
  String created_at;

  Notice({
    required this.id,
    required this.mask_weared_incorrect_count,
    required this.with_mask_count,
    required this.without_mask_count,
    required this.total_count,
    required this.media,
    required this.location,
    required this.created_at,
  });

  factory Notice.fromJson(Map<String, dynamic> json) {
    String formattedCreatedAt = DateFormat('yyyy-MM-dd HH:mm:ss')
        .format(DateTime.parse(json["created_at"]).toLocal());

    var notice = Notice(
      id: json["id"],
      mask_weared_incorrect_count: json["mask_weared_incorrect_count"],
      with_mask_count: json["with_mask_count"],
      without_mask_count: json["without_mask_count"],
      total_count: json["total_count"],
      media: json["media"],
      location: json["location"]["name"],
      created_at: formattedCreatedAt,
    );
    return notice;
  }
}
