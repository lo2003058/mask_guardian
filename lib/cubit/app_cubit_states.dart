import 'package:equatable/equatable.dart';
import 'package:mask_guardian/models/notice.dart';

abstract class CubitStates extends Equatable {
  @override
  List<Object> get props => [];
}

class InitialState extends CubitStates {
  @override
  List<Object> get props => [];
}

class WelcomeStatus extends CubitStates {
  @override
  List<Object> get props => [];
}

class LoadingStatus extends CubitStates {
  @override
  List<Object> get props => [];
}

class LoadedStatus extends CubitStates {
  final List<Notice> notice;
  LoadedStatus(this.notice);
  @override
  // TODO: implement props
  List<Object> get props => [notice];
}
