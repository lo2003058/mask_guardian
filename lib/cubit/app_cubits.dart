import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mask_guardian/cubit/app_cubit_states.dart';
import 'package:mask_guardian/services/data_services.dart';

class AppCubits extends Cubit<CubitStates> {
  AppCubits({required this.data}) : super(InitialState()) {
    emit(WelcomeStatus());
  }

  final DataServices data;
  late final notice;
  late final updatedNotice;

  getNotice(getDataCount) async {
    try {
      emit(LoadingStatus());
      notice = await data.getNotice(getDataCount);
      emit(LoadedStatus(notice));
    } catch (e) {
      print(e);
    }
  }

  updateNotice(getDataCount) async {
    try {
      final state = this.state;
      if(state is LoadedStatus){
        var notice = state.notice;
        notice = await data.getNotice(getDataCount);
        emit(LoadedStatus(notice));
      }
    } catch (e) {
      print(e);
    }
  }

  test() {
    print('test');
  }
}
