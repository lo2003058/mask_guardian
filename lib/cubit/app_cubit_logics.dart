import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mask_guardian/cubit/app_cubit_states.dart';
import 'package:mask_guardian/cubit/app_cubits.dart';
import 'package:mask_guardian/pages/home_nav.dart';
import 'package:mask_guardian/pages/welcome.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppCubitLogics extends StatefulWidget {
  const AppCubitLogics({Key? key}) : super(key: key);

  @override
  State<AppCubitLogics> createState() => _AppCubitLogicsState();
}

class _AppCubitLogicsState extends State<AppCubitLogics> {
  Future<bool> checkIsFirstStart() async {
    final prefs = await SharedPreferences.getInstance();
    final isFirstStart = prefs.getBool('isFirstStart') ?? false;
    return isFirstStart;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<AppCubits, CubitStates>(
        builder: (context, state) {
          if (state is WelcomeStatus || checkIsFirstStart() == true) {
            return WelcomePage();
          }
          // if (checkIsFirstStart() == true) {
          //   return WelcomePage();
          // }
          if (state is LoadedStatus) {
            return HomeNav();
          }
          if (state is LoadingStatus) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }
}
